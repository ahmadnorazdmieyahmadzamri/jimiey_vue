import indexContent from './components/index.vue'
import pointsCollection from './components/pointsCollection'
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
        
   

    {
        name: "pointsCollection",
        component: pointsCollection,
        path: "/",
    },

    {
        name: "indexContent",
        component: indexContent,
        path: "/index-main",
    },
    
]


const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
