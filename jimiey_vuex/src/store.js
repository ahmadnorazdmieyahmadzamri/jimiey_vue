//import Vue from 'vue';
//import Vuex from 'vuex';
import { createStore } from "vuex";

/*const Vue = Vuex(App);
Vue.use(Vuex);*/
export const store = createStore/*new Vuex.Store*/({
    state: {
        collectionList: [],
    },

    mutations: {
        addCollection(state, title){
            state.collectionList=[
                ...state.collectionList,
                {
                    id: Math.random(),
                    title,
                    completed: false,
                },
            ];
        },

        addCollections(state, collection){
            state.collectionList = collection;
        },

        updateCollection(state, collectionId){
            state.collectionList = state.collectionList.map((item) => {
                if(item.id === collectionId) {
                    item.completed = !item.completed;
                }
                return item;
            });
        },

        }
})